from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='site_detail_ingestor',
    version='0.1',
    packages=find_packages(),
    description='A description of my package',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    author='Ricky',
    author_email='horypatrik98@gmail.com',
    url='https://gitlab.com/sourcehaven/mypass/py.website_ingestor',
    install_requires=requirements,
    classifiers=[
        'License :: OSI Approved :: MIT License',
    ],
    python_requires='>=3.6',
)