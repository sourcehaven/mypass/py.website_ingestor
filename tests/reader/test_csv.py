import os
import unittest
from site_detail_ingestor.model import SiteMeta


class TestCsvReader(unittest.TestCase):

    def setUp(self):
        # Get the directory of the current test file
        self.test_dir = os.path.dirname(__file__)
        # Path to the resources folder
        self.resources_dir = os.path.join(self.test_dir, 'resources')

    def test_read_from_file_without_header(self):
        sites = reader.csv.from_file(
            os.path.join(self.resources_dir, "ranked_domains_no_header.csv"),
            site_meta=SiteMeta(rank_index=0, name_index=1),
            header=False,
            delimiter=","
        )
        self.assertEqual(len(sites), 5)

        self.assertEqual(sites[0].rank, 1)
        self.assertEqual(sites[0].name, "fonts.googleapis.com")

        self.assertEqual(sites[-1].rank, 5)
        self.assertEqual(sites[-1].name, "youtube.com")

    def test_read_from_file_with_header(self):
        sites = reader.csv.from_file(
            os.path.join(self.resources_dir, "ranked_domains_header.htm"),
            site_meta=SiteMeta(rank_index=0, name_index=1),
            header=True,
            delimiter=","
        )
        self.assertEqual(len(sites), 4)

        self.assertEqual(sites[0].rank, 1)
        self.assertEqual(sites[0].name, "www.google.com")

        self.assertEqual(sites[-1].rank, 4)
        self.assertEqual(sites[-1].name, "linkedin.com")


if __name__ == '__main__':
    unittest.main()
