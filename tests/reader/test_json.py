import os
import unittest
from model import SiteMeta


class TestJsonReader(unittest.TestCase):

    def setUp(self):
        # Get the directory of the current test file
        self.test_dir = os.path.dirname(__file__)
        # Path to the resources folder
        self.resources_dir = os.path.join(self.test_dir, 'resources')

    def test_read_from_file(self):
        sites = reader.json.from_file(
            os.path.join(self.resources_dir, "ranked_domains.json"),
            site_meta=SiteMeta(rank_index=0, name_index=1)
        )
        self.assertEqual(len(sites), 1000)

        self.assertEqual(sites[0].rank, 1)
        self.assertEqual(sites[0].name, "youtube.com")

        self.assertEqual(sites[-1].rank, 1000)
        self.assertEqual(sites[-1].name, "denofgeek.com")


if __name__ == '__main__':
    unittest.main()
