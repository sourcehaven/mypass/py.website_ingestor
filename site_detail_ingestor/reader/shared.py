from typing import Iterable, Sequence

import requests

from ..model import Site, SiteMeta


def order_sites(sites: Iterable[Site]):
    return sorted(sites, key=lambda site: (site.rank, site.name))


def _generate_sites(rows: Iterable[Sequence[str]], site_meta: SiteMeta):
    for row in rows:
        rank = row[site_meta.rank_index]
        name = row[site_meta.name_index]
        if isinstance(rank, str):
            rank = rank.replace('"', "")
        if isinstance(name, str):
            name = name.replace('"', "")

        yield Site(
            rank=int(rank),
            name=name,
        )


def _read_site(url: str | bytes):
    resp = requests.get(url, verify=False)
    ret = resp.content.decode()
    return ret
