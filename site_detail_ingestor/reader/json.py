import json

from ..model import SiteMeta
from .shared import _read_site, order_sites, _generate_sites


def _generate_dict_values(sites: list[dict]):
    return (list(site.values()) for site in sites)


def from_site(
        path: str,
        site_meta: SiteMeta,
        order=True,
):
    cont = _read_site(path)
    sites: list[dict] = json.loads(cont)

    ret = _generate_sites(_generate_dict_values(sites), site_meta)
    if order:
        ret = order_sites(ret)
    return ret


def from_file(
        path: str,
        site_meta: SiteMeta,
        order=True,
):
    with open(path, "r") as f:
        sites: list[dict] = json.load(f)

    ret = _generate_sites(_generate_dict_values(sites), site_meta)
    if order:
        ret = order_sites(ret)
    return ret
