from typing import Iterable

from ..model import SiteMeta
from .shared import _read_site, order_sites, _generate_sites


def _parse_delimited_text(rows: Iterable[str], site_meta: SiteMeta, header: bool, delimiter=","):
    rows = [row.split(delimiter) for row in rows if len(row) > 0]
    if header:
        rows = rows[1:]

    return _generate_sites(rows, site_meta)


def from_site(
        path: str,
        site_meta: SiteMeta,
        header: bool,
        delimiter=",",
        order=True,
):
    cont = _read_site(path)

    ret = _parse_delimited_text(cont.split("\n"), delimiter=delimiter, header=header, site_meta=site_meta)
    if order:
        ret = order_sites(ret)

    return ret


def from_file(
        path: str,
        site_meta: SiteMeta,
        header: bool,
        delimiter=",",
        order=True,
):
    with open(path, "r") as f:
        ret = _parse_delimited_text(f.readlines(), delimiter=delimiter, header=header, site_meta=site_meta)

    if order:
        ret = order_sites(ret)
    return ret

