from typing import Iterable

from site_detail_ingestor.model import Site


def filter_by_name(sites: Iterable[Site], prefix: str):
    return (site for site in sites if site.name.startswith(prefix))
