import dataclasses


@dataclasses.dataclass
class SiteMeta:
    rank_index: int
    name_index: int
