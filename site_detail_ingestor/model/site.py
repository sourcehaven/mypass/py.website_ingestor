import dataclasses


@dataclasses.dataclass
class Site:
    rank: int
    name: str
